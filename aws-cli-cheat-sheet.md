# AWS CLI Cheatsheet

#### Created a named profile

```
aws configure --profile personal
```

## Interact with AWS by providing specifying the profile

- Below is an example where "personal" is the name of the profile.

```
aws s3 ls --profile personal
```

#### Set Default Profile

-You can also set the default profile as an environment varialble so that you dont need to always specify the profile in the command. This will be active only for the current shell session.

```
export AWS_DEFAULT_PROFILE=personal
```

#### List S3 Buckets

```
aws s3 ls
```

#### List out AWS AZs [With respect to region specified under ~/.aws/config (if any)]:

```
aws ec2 describe-availability-zones | jq
```

Sample Outputs:
```
{
    "AvailabilityZones": [
        {
            "State": "available", 
            "ZoneName": "ap-south-1a", 
            "Messages": [], 
            "ZoneId": "aps1-az1", 
            "RegionName": "ap-south-1"
        }, 
        {
            "State": "available", 
            "ZoneName": "ap-south-1b", 
            "Messages": [], 
            "ZoneId": "aps1-az3", 
            "RegionName": "ap-south-1"
        }
    ]
}
```

#### List regions

```
aws ec2 describe-regions | jq ".Regions[].RegionName"
```

-Outputs:

```
"eu-north-1"
"ap-south-1"
"eu-west-3"
"eu-west-2"
"eu-west-1"
"ap-northeast-2"
"ap-northeast-1"
"sa-east-1"
"ca-central-1"
"ap-southeast-1"
"ap-southeast-2"
"eu-central-1"
"us-east-1"
"us-east-2"
"us-west-1"
"us-west-2"
```


List VPCs

aws ec2 describe-vpcs 
